import json
import requests
from .keys import PEXELS_API_KEY, WEATHER_API_KEY



def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    #authorization header is specific for Pexels
    headers = {"Authorization": PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    url = "https://api.pexels.com/v1/search/"
    query = {"query": f"{city}, {state}"}
    # Make the request
    response = requests.get(url, params=query, headers=headers)
    # Parse the JSON response
    content = json.loads(response.content)
    # Return a dictionary that contains a `picture_url` key and
    try:
        picture_url = content["photos"][0]["src"]["original"]
    #   one of the URLs for one of the pictures in the response
        return {"picture_url": picture_url}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    #copy the api call url up to the ?
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {"q": f"{city}, {state}", "appid": WEATHER_API_KEY}
    # Make the request
    response = requests.get(url, params=params)
    # Parse the JSON response
    content = response.json()
    # Get the latitude and longitude from the response
    lat = content[0]["lat"]
    lon = content[0]["lon"]

    # Create the URL for the current weather API with the latitude
    #  and longitude
    url = "https://api.openweathermap.org/data/2.5/weather"
    #set params according to the url specifications
    params = {"lat": lat, "lon": lon, "units": "imperial", "appid": WEATHER_API_KEY}
        # Make the request
    response = requests.get(url, params=params)
    # Parse the JSON response
    content = response.json()
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    description = content["weather"][0]["description"]
    temp = content["main"]["temp"]
    #Return the dictionary
    return {"temp": temp, "description": description}
